const templates = new Map()

async function getTemplate (url) {
  if (templates.has(url)) {
    const template = templates.get(url)
    const clone = document.importNode(template.content, true)
    return clone
  } else {
    const response = await window.fetch(url, {
      mode: 'cors',
      credentials: 'include'
    })
    if (!response.ok) {
      throw new Error(
        `Failed to load template: ${url} (Status: ${response.status})`
      )
    }
    const html = await response.text()
    const template = document.createElement('template')
    template.innerHTML = html
    templates.set(url, template)
    const clone = document.importNode(template.content, true)
    return clone
  }
}

function onLoad (resource) {
  return new Promise((resolve) => {
    function done () {
      resource.removeEventListener('load', done)
      resource.removeEventListener('error', done)
      resolve()
    }
    resource.addEventListener('load', done)
    resource.addEventListener('error', done)
  })
}

class TemplateElement extends HTMLElement {
  async connectedCallback () {
    if (this.template === undefined) {
      throw new Error('Missing .template property')
    }
    this.setAttribute('hidden', true)
    const root = this.attachShadow({ mode: 'open' })
    const template = await getTemplate(this.template)
    root.appendChild(template)
    const stylesheets = root.querySelectorAll(
      'style[src], link[rel=stylesheet]'
    )
    await Promise.all(Array.from(stylesheets).map(onLoad))
    this.removeAttribute('hidden')
  }
}

export default TemplateElement
